# Be aware of AWS Sandbox limits:
# https://support.linuxacademy.com/hc/en-us/articles/360025783132-What-can-I-do-with-the-Cloud-Playground-AWS-Sandbox-

provider "aws" {
  region = "${var.region}"
}

//terraform {
//  required_version = ">= 0.11.14"
//  backend          "s3"             {}
//}

module "label" {
  source  = "git::https://github.com/cloudposse/terraform-terraform-label.git?ref=tags/0.2.1"

  namespace  = "${var.namespace}"
  name       = "${var.name}"
  stage      = "${var.stage}"
  delimiter  = "${var.delimiter}"
  attributes = "${var.attributes}"
  tags       = "${var.tags}"
}

#-----
# DATA
#-----

# You can use LinuxAcademy public VPC if you like
# https://www.terraform.io/docs/providers/aws/d/vpcs.html

data "aws_caller_identity" "this" {}

data "aws_vpcs" "public" {
  tags = {
    Network = "Public"
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

#----
# VPC
#----

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "1.60.0"

  name = "${module.label.id}"

  cidr = "${var.cidr}"

  azs             = "${var.availability_zones}"
  private_subnets = "${var.private_subnets}"
  public_subnets  = "${var.public_subnets}"
  intra_subnets   = "${var.intra_subnets}"

  assign_generated_ipv6_cidr_block = "${var.assign_generated_ipv6_cidr_block}"

  enable_nat_gateway     = "${var.enable_nat_gateway}"
  single_nat_gateway     = "${var.single_nat_gateway}"
  one_nat_gateway_per_az = "${var.one_nat_gateway_per_az}"

  enable_dns_support   = "${var.enable_dns_support}"
  enable_dns_hostnames = "${var.enable_dns_hostnames}"

  vpc_tags = "${module.label.tags}"

  private_subnet_tags = {
    Tier = "private"
  }

  public_subnet_tags = {
    Tier = "public"
  }
}

#----
# EC2
#----

resource "aws_security_group" "this" {
  name_prefix = "${module.label.id}${var.delimiter}"
  vpc_id      = "${module.vpc.vpc_id}"

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"

    # You should allow only CIDR blocks that you trust
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${module.label.tags}"
}

module "key_pair" {
  source = "git::https://github.com/cloudposse/terraform-aws-key-pair.git?ref=tags/0.3.2"

  namespace             = "${var.namespace}"
  stage                 = "${var.stage}"
  name                  = "${var.name}"
  ssh_public_key_path   = "${path.module}/secrets"
  generate_ssh_key      = "true"
  private_key_extension = ".pem"
  public_key_extension  = ".pub"
  chmod_command         = "chmod 600 %v"
}

resource "aws_instance" "this" {
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "${var.instance_type}"

  vpc_security_group_ids = ["${aws_security_group.this.id}"]
  subnet_id              = "${module.vpc.public_subnets[0]}"
  key_name               = "${module.key_pair.key_name}"

  credit_specification {
    cpu_credits = "standard"
  }

  tags        = "${module.label.tags}"
  volume_tags = "${module.label.tags}"

  depends_on = ["aws_security_group.this"]
}
